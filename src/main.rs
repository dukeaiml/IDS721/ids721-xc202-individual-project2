use actix_web::{get, web, App, HttpResponse, HttpServer, Responder};
use serde_json::to_string;
use serde::{Serialize};
use std::collections::HashMap;

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello! Please add [/palindrome?string={input}] at the end of the URL for deciding whether the input string is a palindrome")
}

#[derive(Serialize)]
struct PalindromeResponse {
    string: String,
    is_palindrome: bool,
}

fn is_palindrome(s: &str) -> bool {
    let s = s.chars().filter(|c| c.is_alphanumeric()).collect::<String>();
    let reversed = s.chars().rev().collect::<String>();
    s.eq_ignore_ascii_case(&reversed)
}

#[get("/palindrome")]
async fn echo(web::Query(info): web::Query<HashMap<String, String>>) -> impl Responder {
    match info.get("string") {
        Some(input) => {
            let is_palindrome = is_palindrome(&input);
            let message = if is_palindrome {
                format!("'{}' is a palindrome!", &input)
            } else {
                format!("'{}' is not a palindrome.", &input)
            };
            let response = PalindromeResponse {
                string: message,
                is_palindrome,
            };
            HttpResponse::Ok().body(to_string(&response).unwrap())
        }
        None => HttpResponse::BadRequest().body("Missing string parameter"),
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(hello)
            .service(echo)
    })
        .bind("0.0.0.0:8088")?
        .run()
        .await
}
