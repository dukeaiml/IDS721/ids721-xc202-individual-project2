## Rust Microservice

This individual project was based on mini-project 2 and was designed to build a simple rust actix web application, with Docker making it to be containerized and running in the container locally. **Rust** (https://www.rust-lang.org/) and **Docker** (https://www.docker.com/) are required to start this project.

### 1. Rust Code Overview

This is an asynchronous function named `hello()` decorated with `#[get("/")]` attribute, indicating that it handles HTTP GET requests for the root path ("/"). It returns an object implementing the `Responder` trait. Inside the function, an HTTP response object is constructed using `HttpResponse::Ok().body()`, which returns a simple welcome message.

```rust
#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello! Please add [/palindrome?string={input}] at the end of the URL for deciding whether the input string is a palindrome")
}
```

This is a regular function `is_palindrome()` used to check if a given string is a palindrome. It filters out non-alphanumeric characters from the input string, then reverses it, and finally checks if the reversed string is equal to the original one, ignoring case.

```rust
fn is_palindrome(s: &str) -> bool {
    let s = s.chars().filter(|c| c.is_alphanumeric()).collect::<String>();
    let reversed = s.chars().rev().collect::<String>();
    s.eq_ignore_ascii_case(&reversed)
}
```
This is another asynchronous function `echo()` decorated with `#[get("/palindrome")]` attribute, indicating that it handles HTTP GET requests for the "/palindrome" path. It accepts a query parameter named info and parses it into a `HashMap<String, String>` using `web::Query()`. Inside the function, it checks if the query parameter named string is present. If it exists, it calls the `is_palindrome()` function to check if the input string is a palindrome, constructs a `PalindromeResponse` struct to represent the result, converts it to a JSON string, and returns it as an HTTP response. If the `string` parameter is missing, it returns a HTTP 400 error.

```rust
#[get("/palindrome")]
async fn echo(web::Query(info): web::Query<HashMap<String, String>>) -> impl Responder {
    match info.get("string") {
        Some(input) => {
            let is_palindrome = is_palindrome(&input);
            let message = if is_palindrome {
                format!("'{}' is a palindrome!", &input)
            } else {
                format!("'{}' is not a palindrome.", &input)
            };
            let response = PalindromeResponse {
                string: message,
                is_palindrome,
            };
            HttpResponse::Ok().body(to_string(&response).unwrap())
        }
        None => HttpResponse::BadRequest().body("Missing string parameter"),
    }
}
```

### 2. Docker Overview
Docker is a platform for building, deploying, and running applications in containers. Containers are lightweight, portable, and consistent environments that encapsulate an application and its dependencies. Docker uses a client-server architecture, where the Docker client communicates with the Docker daemon to manage containers.

#### Dockerfile Explanation:
The Dockerfile first builds the dependencies to speed up the process and then builds the application itself. The final image is based on a slim Ubuntu image, and it copies the built binary into the container, exposing port 8088 and defining the command to run the application. The following steps show the details:
* Specify the base image as the latest Rust image and names it '**builder**'.
* Set the working directory within the container to `/usr/src/app`.
* Copy the local `Cargo.toml` and `Cargo.lock` files into the container.
* Build the application within the container in release mode.
* Change the base image to the latest **slim** Ubuntu image for the final container.
* Set the working directory within the final container to `/usr/src/app`.
* Copy the built binary from the '**builder**' image to the final container.
* Inform Docker that the application inside the container will listen on port **8088**.
* Specify the command to run the application when the container starts.

### 3. Build Docker Image
Since the Dockerfile has been defined, we can now use the command `docker build -t rust-microservice .` to build the image based on the Dockerfile. The code in Dockerfile is resolved line by line. The size of the final image is much less than which of the latest Rust image:

![Screenshot 2024-03-19 at 7.50.40 PM.png](screenshots%2FScreenshot%202024-03-19%20at%207.50.40%20PM.png)

I am using a Docker Desktop for Mac. It provides better visualization to see the images and containers created. We can see that an image `rust-microservice` is created after the pre-mentioned command is run. This image contains the basic environment for the web app to run and is slight for deployment.

![Screenshot 2024-03-19 at 7.46.24 PM.png](screenshots%2FScreenshot%202024-03-19%20at%207.46.24%20PM.png)

### 4. Run the Container Locally
We can start using the command `docker run -d -p 8088:8088 rust-microservice` to build a container based on the image `rust-microservice` in the detached mode. The web application is providing service on the port `8088` in the container, mapping 8088 port of the container to 8088 port of the host, allowing users to access the website locally:

![Screenshot 2024-03-19 at 7.46.09 PM.png](screenshots%2FScreenshot%202024-03-19%20at%207.46.09%20PM.png)

Now that we can run the container `inspiring_babbage`, and the web application should be listening on http://localhost:8088. The following figure shows what happens when accessing the website:

![Screenshot 2024-03-19 at 7.46.47 PM.png](screenshots%2FScreenshot%202024-03-19%20at%207.46.47%20PM.png)

We need to add `/palindrome?string={input}` at the end of the URL to invoke the function:

![Screenshot 2024-03-19 at 7.47.17 PM.png](screenshots%2FScreenshot%202024-03-19%20at%207.47.17%20PM.png)

![Screenshot 2024-03-19 at 7.47.05 PM.png](screenshots%2FScreenshot%202024-03-19%20at%207.47.05%20PM.png)

You can test by curl alternatively:

![Screenshot 2024-03-19 at 10.37.33 PM.png](screenshots%2FScreenshot%202024-03-19%20at%2010.37.33%20PM.png)

### 3. CI/CD pipeline
The following configuration is a GitLab CI/CD setup for building and deploying a Rust microservice. It consists of the following components:

1. **Stages**: Only one stage named `build` is defined. Stages organize jobs into logical groups within the pipeline.

2. **Variables**: Global variables are defined for reuse across jobs. These include the Docker image tag, Docker host address, and Docker storage driver.

3. **Build Job**: This job, named `build`, runs in the `build` stage. It uses the Docker image `docker:stable` to execute Docker commands. Docker-in-Docker service (`docker:dind`) is enabled for building Docker images. Before the script execution, it logs in to the Docker Registry using credentials provided as environment variables. The script section contains commands for building the Rust microservice Docker image, running the container, and listing containers.

```
# Define stages for the pipeline
stages:
  - build

# Define variables that can be used across jobs
variables:
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2

build:
  stage: build
  image: docker:stable  # Use the Docker image for running Docker commands
  services:
    - docker:dind  # Enable Docker-in-Docker service for building Docker images
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker build -t rust-microservice .
    - docker run -d -p 8088:8088 rust-microservice
    - docker ps -a
```

### 4. Demo Video
You can access the demo video here:

[Demo Video.mov](Demo%20Video.mov)